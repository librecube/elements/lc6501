FROM python:3.9-slim

RUN apt-get update &&\
    apt-get install -y git &&\
    rm -rf /var/lib/apt/lists/*

RUN useradd dummy

WORKDIR /opt
COPY requirements.txt /opt
RUN pip3 install -r requirements.txt
RUN pip3 install gunicorn
COPY run.py /opt
ADD application /opt/application

USER dummy
